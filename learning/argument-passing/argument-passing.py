# "Everything in Python is an object"
#
# Identifiers (names) in Python are references to objects.
# Some objects are mutable, some are immutable.
#
# At the same time, everything in Python is passed by copy (that is,
# in Python's case, by copy of a reference).
# 
# So, when you pass an object to a function (subroutine), what really happens
# is that the reference to this object is copied and passed to the subroutine.
# 
# So in the subroutine you can modify the referenced object (if it's mutable),
# but you can't reassign the original reference (which is inaccessible) -
# you only work with a local copy.
# 
# If you want to "reassign" the original reference (which is the only way how 
# to "change" immutable objects), you must create a new object and return
# the new reference to it.
#
# This concept is better understood if you are familiar with the concept of
# pointers and references in C/C++.
#-------------------------------------------------------------------------------
# "lst" is a local copy of the original reference to the list.
# So "lst" points to the original location, but it's not the same reference as
# the original one.
def mutateList ( lst ):
    # modifying the referenced object - object is mutable => OK
    lst . append ( 666 )
    
    # modifying the referenced object - object is mutable => OK
    lst . extend ( [777] )
    
    # reassigning the local "lst" variable to a newly initialized list
    # this change is only local and temporary, working only in this function
    # (unless we return the local reference and the return value is accepted)
    lst = [ 333 ]
    
    # returning the local variable ( lst pointing to the list "[333]" )
    return lst;
#-------------------------------------------------------------------------------
def main ():
    # initialize an empty list
    lst = []
    
    # empty list
    print ( lst )
    
    # call the mutateList function but throw away (ignore) the return value
    mutateList ( lst )

    # [666, 777]
    print ( lst )

    # accept the return value = reassign "lst" to the newly returned value
    lst = mutateList ( lst )

    # [333]
    print ( lst )
#-------------------------------------------------------------------------------
main ()
